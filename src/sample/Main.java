package sample;

import javafx.application.Application;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void init() throws Exception {
        System.out.println("Initialization");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println("Starting");
        VBox leftColumn = new VBox();
        Label label1 = new Label("Label on the left");
        label1.setText("New test for the label on the left");
        TextField textField1 = new TextField();
        textField1.setText("New text for empty text field");
        textField1.setOnKeyPressed(keyEvent -> {
            System.out.println("Key pressed event: " + keyEvent.getCode());
        });

        textField1.setOnKeyReleased(keyEvent -> {
            System.out.println("Key released evnet: " + keyEvent.getCode());
        });


        leftColumn.getChildren().addAll(label1, textField1);

        VBox rightColumn = new VBox();
        ObservableList<Node> rightColumnNodes = rightColumn.getChildren();
        rightColumnNodes.add(new Label("Label on the right"));
        rightColumnNodes.add(new TextField("Text Feild on the right"));

        Button button = new Button("Click on me NOW!");
        rightColumnNodes.add(button);
        button.setOnAction((actionEvent -> {
            System.out.println("Button was clicked");
        }));

        CheckBox checkBox = new CheckBox("Tick me off!");
        checkBox.setAllowIndeterminate(true);
        checkBox.setIndeterminate(true);
//        checkBox.setSelected(true);

        rightColumnNodes.add(checkBox);
        TextArea textArea = new TextArea("Text area");
        rightColumnNodes.add(textArea);

        ListView<String> listView = new ListView<>();

        ObservableList<String> items = listView.getItems();
        items.add("First element");
        items.add("Second element");
        items.add("Third element");
        rightColumnNodes.add(listView);


        HBox hbox = new HBox();
        hbox.getChildren().addAll(leftColumn, rightColumn);

//        GridPane gridPane = new GridPane();
//
//        gridPane.add(new Label("Label on the left"), 0, 0);
//        gridPane.add(new TextField(), 0, 1);
//        gridPane.add(new Label("Label on the right"), 1, 0);
//        gridPane.add(new TextField("Text Feild on the right"), 1, 1);


        System.out.println(textField1.getText());

        StringProperty textFieldProperty = textField1.textProperty();
        textFieldProperty.addListener(((observableValue, oldValue, newValue) -> {
            System.out.println("text was changed from " + oldValue + " to " + newValue);
        }));

        textFieldProperty.bindBidirectional(label1.textProperty());

        Slider slider = new Slider();
        rightColumn.spacingProperty().bindBidirectional(slider.valueProperty());

        rightColumnNodes.add(slider);

        Scene scene = new Scene(hbox);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        System.out.println("Stopping application");
    }

    public static void main(String[] args) {
        launch(args);
    }
}
